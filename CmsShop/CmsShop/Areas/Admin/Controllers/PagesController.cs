﻿using CmsShop.Models.Data;
using CmsShop.Models.ViewModels.Pages;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CmsShop.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class PagesController : Controller
    {
        // GET: Admin/Pages
        public ActionResult Index()
        {
            //Declare List of PageVM
            List<PageVM> pagesList;

            //Inicialization List(by data)

            using (Db db = new Db())
            {
                //download list of pages from DataBAse sort by orting field
                pagesList = db.Pages.ToArray().OrderBy(x => x.Sorting).Select(x => new PageVM(x)).ToList();
                //we crete also new PageVm to sent to the View.
                //Thanks constructor in PageVm we must only get one row from db, and all field are inicialize (x-> it a row download form db)

            }

            //rReturn pages to View
            return View(pagesList);
        }

        // GET: Admin/Pages/AddPage
        [HttpGet]
        public ActionResult AddPage()
        {
            return View();
        }

        // POST: Admin/Pages/AddPage
        [HttpPost]
        public ActionResult AddPage(PageVM model)
        {
            //Checking model state(stanu formularza)
            if (!ModelState.IsValid)
            {
                //if not valid
                //return existing view(Form with users data) with validation form
                return View(model);
            }

            using (Db db = new Db())
            {
                //slug is responsible for page adress
                string slug;
                //initializaction PageDTO
                PageDTO dto = new PageDTO();

                //slug issue
                //slug is decccribed by title
                if (string.IsNullOrWhiteSpace(model.Slug))
                {
                    //default slag(user not write a special page name)
                    slug = model.Title.Replace(" ", "-").ToLower();
                }
                else
                {
                    //if user write a special slag(adress www)
                    slug = model.Slug.Replace(" ", "-").ToLower();
                }

                //Title and Slug it must be uniqe!!!!!
                //Prevent to duble page Name
                if (db.Pages.Any(x => x.Title == model.Title) || db.Pages.Any(x => x.Slug == model.Slug))
                {
                    //if page name exist in db 
                    //error!!!
                    ModelState.AddModelError("", "Ten tytuł lub adress strony już istieje");
                    return View(model);
                }

                //We assign another value from model(form) to Db(by DTO)
                dto.Title = model.Title;
                dto.Slug = slug;
                dto.Body = model.Body;
                dto.HasSidebar = model.HasSidebar;
                dto.Sorting = 1000;

                //Save data(from dto) to DB
                db.Pages.Add(dto);
                db.SaveChanges();

            }

            TempData["SM"] = "Dodałeś nową stronę";

            return RedirectToAction("AddPage");
        }

        // GET: Admin/Pages/EditPage
        [HttpGet]
        public ActionResult EditPage(int id)
        {
            //declaration PageViewModel (PageVM) - model
            PageVM model;

            using (Db db = new Db())
            {
                //Declare PageDto becuse we wanna to download page from DB(data base)
                PageDTO dto = db.Pages.Find(id);
                //CHeck if page exist
                if (dto == null)
                {
                    return Content("Strona nie istnije");
                }

                //else... SO if Page exist in Db
                model = new PageVM(dto);


            }

            return View(model);
        }

        // POST: Admin/Pages/EditPage
        [HttpPost]
        public ActionResult EditPage(PageVM model)
        {

            //check model State(proper fill all fields in form)
            if (!ModelState.IsValid)
            {
                //if no valid return form
                return View(model);
            }

            //else ... do
            using (Db db = new Db())
            {
                //download page Id 
                int id = model.Id;

                //Slug iniciatizaytion
                string slug = "home";

                //download Page to edit
                PageDTO dto = db.Pages.Find(id);


                //blocking Slug field edition when slug == home
                if (model.Slug != "home")
                {
                    //check if field slag is null or whitespace
                    if (string.IsNullOrWhiteSpace(model.Slug))
                    {
                        slug = model.Title.Replace(" ", "-").ToLower();
                    }
                    else
                    {
                        slug = model.Slug.Replace(" ", "-").ToLower();
                    }
                }

                //check if web page and adress are unique
                if (db.Pages.Where(x => x.Id != id).Any(x => x.Title == model.Title) ||
                    db.Pages.Where(x => x.Id != id).Any(x => x.Slug == slug))
                {
                    ModelState.AddModelError("", "Strona lub adres strony już istnieje!");
                }

                // mofyfication next DTO fields
                dto.Title = model.Title;
                dto.Slug = slug;
                dto.HasSidebar = model.HasSidebar;
                dto.Body = model.Body;

                //Save edit page to database(DB)
                db.SaveChanges();

            }

            //config TempData message
            TempData["SM"] = "Wyedytowałeś stronę";

            //Redirect
            return RedirectToAction("EditPage");
        }

        // GET: Admin/Pages/Details/id
        [HttpGet]
        public ActionResult Details(int id)
        {
            //declare ViewModel(PageVM)
            PageVM model;

            using(Db db = new Db())
            {
                //download page o id
                PageDTO dto = db.Pages.Find(id);
                //CHeck if page with this id exist
                if(dto == null)
                {
                    //if not exists dto ==null
                    return Content("Strona o danym id nie iestnieje");
                }

                //ellse exist .....
                //initializacion PageVM
                model = new PageVM(dto);
            }


            return View(model);
        }

        // GET: Admin/Pages/Delete/id
        [HttpGet]
        public ActionResult Delete(int id)
        {
            using(Db db= new Db())
            {
                // download page to delete
                PageDTO dto = db.Pages.Find(id);

                //delete a page from DataBAse(DB)
                db.Pages.Remove(dto);

                //save changes
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }


        // POST: Admin/Pages/ReorderPages
        [HttpPost]
        public  ActionResult ReorderPages(int[] id)
        {
            //This method reorder pages by Drag and Drop functionality
            using(Db db = new Db())
            {
                int count = 1;
                PageDTO dto;

                // sorting pages, save to Database(Db)
                foreach (var pageId in id)
                {
                    dto = db.Pages.Find(pageId);
                    dto.Sorting = count;

                    db.SaveChanges();
                    count++;
                }

            }

            return View();
        }

        // GET: Admin/Pages/EditSidebar
        [HttpGet]
        public ActionResult EditSidebar()
        {
            //declare SidebarVm(View Model)
            SidebarVM model;

            using (Db db = new Db())
            {
                //download SidebarDto(w have only one with no.1)
                SidebarDTO dto = db.Sidebar.Find(1);

                //Initialization model
                model = new SidebarVM(dto);

            }

            return View(model);
        }

        // POST: Admin/Pages/EditSidebar
        [HttpPost]
        public ActionResult EditSidebar(SidebarVM model)
        {
            using (Db db = new Db())
            {
                //donwload SideBar DTO from DataBAse
                SidebarDTO dto = db.Sidebar.Find(1);

                //modyfication Sidebar
                dto.Body = model.Body;

                //save to DB
                db.SaveChanges();
            }

            //Message about modyfiation Sidebar
            TempData["SM"] = "Zmodyfikowałeś Pasek Boczny";


            return RedirectToAction("EditSidebar");
        }

    }
}