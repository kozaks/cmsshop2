﻿using CmsShop.Areas.Admin.Models.ViewModels.Shop;
using CmsShop.Models.Data;
using CmsShop.Models.ViewModels.Shop;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace CmsShop.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ShopController : Controller
    {
        // GET: Admin/Shop/Categories
        public ActionResult Categories()
        {
            //Declare List Categories to display in View
            List<CategoryVM> categotyCMList;

            using (Db db = new Db())
            {
                categotyCMList = db.Categories
                    .ToArray()
                    .OrderBy(s => s.Sorting)
                    .Select(x => new CategoryVM(x)).ToList();
            }


            return View(categotyCMList);
        }

        //POST: Admin/Shop/AddNewCategory
        [HttpPost]
        public string AddNewCategory(string catName)
        {

            // Deklaracja id
            string id;

            using (Db db = new Db())
            {
                // sprawdzenie czy nazwa kategorii jest unikalna
                if (db.Categories.Any(x => x.Name == catName))
                    return "tytulzajety";

                // Inicjalizacja DTO
                CategoryDTO dto = new CategoryDTO();
                dto.Name = catName;
                dto.Slug = catName.Replace(" ", "-").ToLower();
                dto.Sorting = 1000;

                // zapis do bazy
                db.Categories.Add(dto);
                db.SaveChanges();

                // pobieramy id
                id = dto.Id.ToString();
            }

            return id;
        }

        //POST: Admin/Shop/ReorderCategories
        [HttpPost]
        public ActionResult ReorderCategories(int[] id)
        {
            using (Db db = new Db())
            {
                // inicjalizacja licznika
                int count = 1;

                // deklaracja DTO
                CategoryDTO dto;

                // sortowanie kategorii
                foreach (var catId in id)
                {
                    dto = db.Categories.Find(catId);
                    dto.Sorting = count;

                    // zapis na bazie
                    db.SaveChanges();

                    count++;
                }
            }

            return View();
        }

        //GET: Admin/Shop/DeleteCategory
        [HttpGet]
        public ActionResult DeleteCategory(int id)
        {

            using(Db db= new Db())
            {

                //
                CategoryDTO dto = db.Categories.Find(id);

                //delete category
                db.Categories.Remove(dto);

                //save todb
                db.SaveChanges();

            }

            return RedirectToAction("Categories");

        }
        [HttpGet]
        public string RenameCategory(string newCatName, int id)
        {
            using(Db db = new Db())
            {
                //checking unique category name
                if (db.Categories.Any(x => x.Name == newCatName))
                    return "tytulzajety";

                //else
                //dowlload this category by id
                CategoryDTO dto = db.Categories.Find(id);

                //category edit
                dto.Name = newCatName;
                dto.Slug = newCatName.Replace(" ", "-").ToLower();

                db.SaveChanges();

            }

            return "ok";
        }


        //Empty form for add product
        // GET:Admin/Shop/AddProduct
        [HttpGet]    //Show form 
        public ActionResult AddProduct()
        {
            //Initialization model
            ProductVM model = new ProductVM();

            //fetch list od categories
            using(Db db = new Db())
            {
                model.Categories = new SelectList(db.Categories.ToList(), "Id", "Name");
            }

            return View(model);
        }

        //Form to adding a new Product to the shop 
        // POST:Admin/Shop/AddProduct
        [HttpPost]
        public ActionResult AddProduct(ProductVM model, HttpPostedFileBase file)
        {
            //check model state
            if (!ModelState.IsValid)
            {
                using(Db db = new Db())
                {
                    model.Categories = new SelectList(db.Categories.ToList(), "Id", "Name");
                    return View(model);
                }
             
            }

            //check uniqe product Name
            using (Db db = new Db())
            {

                if(db.Products.Any(x =>x.Name == model.Name))
                {
                    model.Categories = new SelectList(db.Categories.ToList(), "Id", "Name");
                    ModelState.AddModelError("", "Ta nazwa produktu jest juz zajeta!");
                    return View(model);

                }

            }

            //declare product Id
            int id;

            //Adding product and save to database
            using(Db db = new Db())
            {
                ProductDTO product = new ProductDTO();
                product.Name = model.Name;
                product.Slug = model.Name.Replace(" ", "-");
                product.Description = model.Description;
                product.Price = model.Price;
                product.CategoryId = model.CategoryId;

                //fetch category by CategoryId from model
                CategoryDTO catDto = db.Categories.FirstOrDefault(x => x.Id == model.CategoryId);
                product.CategoryName = catDto.Name; //

                db.Products.Add(product);
                db.SaveChanges();

                //Fetched id 
                id = product.Id;

            }

            //Set a message
            TempData["SM"] = "Dodałeś produkt";

            #region Upload Image

            //Catalog structure
            var originalDirectory = new DirectoryInfo(string.Format("{0}Images\\Uploads", Server.MapPath(@"\")));

            var pathStrings1 = Path.Combine(originalDirectory.ToString(), "Products");
            var pathStrings2 = Path.Combine(originalDirectory.ToString(), "Products\\" + id.ToString());
            var pathStrings3 = Path.Combine(originalDirectory.ToString(), "Products\\" + id.ToString() + "\\Thumbs");
            var pathStrings4 = Path.Combine(originalDirectory.ToString(), "Products\\" + id.ToString() + "\\Gallery");
            var pathStrings5 = Path.Combine(originalDirectory.ToString(), "Products\\" + id.ToString() + "\\Gallery\\Thumbs" );

            //check existing od cataloge
            if (!Directory.Exists(pathStrings1))
                Directory.CreateDirectory(pathStrings1);
            if (!Directory.Exists(pathStrings2))
                Directory.CreateDirectory(pathStrings2);
            if (!Directory.Exists(pathStrings3))
                Directory.CreateDirectory(pathStrings3);
            if (!Directory.Exists(pathStrings4))
                Directory.CreateDirectory(pathStrings4);
            if (!Directory.Exists(pathStrings5))
                Directory.CreateDirectory(pathStrings5);

            //check existing file
            if(file != null && file.ContentLength> 0)
            {
                string extension = file.ContentType.ToLower();

                if(extension != "image/jpg" &&
                    extension != "image/jpeg" &&
                    extension != "image/pjpeg" &&
                    extension != "image/gif" &&
                    extension != "image/x-png" &&
                    extension != "image/png")
                {
                    using (Db db = new Db())
                    {
                        model.Categories = new SelectList(db.Categories.ToList(), "Id", "Name");
                        ModelState.AddModelError("", "Obraz nie został przesłany - nieprawidłowe rozszerzenie obrazu. ");
                        return View(model);
                    }

                }

                //inicjalization image naame
                string imageName = file.FileName;

                //save image name to data base
                using(Db db = new Db())
                {
                    ProductDTO dto = db.Products.Find(id);
                    dto.ImageName = imageName;
                    db.SaveChanges();
                }

                //Save image to the cataloge structures
                //1 save image to cataloge Products
                var path = string.Format("{0}\\{1}", pathStrings2, imageName);
                //2 save miniature(thumb) to cataloge
                var path2 = string.Format("{0}\\{1}", pathStrings3, imageName);

                //saving image
                file.SaveAs(path);
                //saving thumbs
                WebImage img = new WebImage(file.InputStream);
                img.Resize(200, 200);
                img.Save(path2);
                
            }


            #endregion


            return RedirectToAction("AddProduct");
        }



        //Thanks that we get List of all products avoiale in Shop
        // GET:Admin/Shop/Products
        [HttpGet]
        public ActionResult Products(int? page, int? catId)
        {
            //declare list of products
            List<ProductVM> listOfProductVM;

            //set a new page number (PageListMVC - nuget)
            var pageNumber = page ?? 1; //chosen page number or default 1

            //
            using (Db db = new Db())
            {
                //Inicialiazation list of product(fetching Products)
                listOfProductVM = db.Products.ToArray()
                    .Where(x => catId == null || catId == 0 || x.CategoryId == catId)  //all categories or defined by user
                    .Select(x => new ProductVM(x))
                    .ToList();

                //fetch list of categories to dropDownList
                ViewBag.Categories = new SelectList(db.Categories.ToList(), "Id", "Name");

                //set choosen Category
                ViewBag.SelectedCat = catId.ToString();
            }

                //Setting a pagedlist
                var OnePageOfProducts = listOfProductVM.ToPagedList(pageNumber, 3);
                ViewBag.OnePageOfProducts = OnePageOfProducts;

                return View(listOfProductVM);

            
            
        }


        // We get View Model from db with data to selected Product
        // GET:Admin/Shop/EditProduct/id
        [HttpGet]
        public ActionResult EditProduct(int id)
        {
            //declare productVm
            ProductVM model;

            using(Db db = new Db())
            {

                //fetch product to edit
                ProductDTO dto = db.Products.Find(id);

                //check if product exist
                if(dto == null)
                {
                    return Content("Ten produkt nie istnieje");
                }

                //
                //initialization model
                model = new ProductVM(dto);

                //fetching list of Categories
                model.Categories = new SelectList(db.Categories.ToList(), "Id", "Name");
                //setting photo(get file name)
                model.GalleryImages = Directory.EnumerateFiles(Server.MapPath("~/Images/Uploads/Products/" + id + "/Gallery/Thumbs"))
                                              .Select(fn => Path.GetFileName(fn));


            }


            return View(model);
        }

        //Tanks that post method we can modyfie Products and save it to db
        // POST:Admin/Shop/EditProduct
        [HttpPost]
        public ActionResult EditProduct(ProductVM model, HttpPostedFileBase file)
        {
            //fetch product id
            int id = model.Id;

            //fetch category to dropDown list
            using(Db db = new Db())
            {
                model.Categories = new SelectList(db.Categories.ToList(), "Id", "Name");
            }

            //set a image(fetch)
            model.GalleryImages = Directory.EnumerateFiles(Server.MapPath("~/Images/Uploads/Products/" + id + "/Gallery/Thumbs"))
                                            .Select(fn => Path.GetFileName(fn));
            //check ModelState
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            //Check uniqe name
            using(Db db = new Db())
            {
                if(db.Products.Where(x =>x.Id !=id).Any(x => x.Name == model.Name))
                {
                    ModelState.AddModelError("", "Ta nazwa jest już zajęta");
                    return View(model);
                }
            }

            //Edit product and saving to db
            using (Db db = new Db())
            {
                ProductDTO dto = db.Products.Find(id);
                dto.Name = model.Name;
                dto.Slug = model.Name.Replace(" ", "-").ToLower();
                dto.Description = model.Description;
                dto.Price = model.Price;
                dto.CategoryId = model.CategoryId;
                dto.ImageName = model.ImageName;

                CategoryDTO catDto = db.Categories.FirstOrDefault(x => x.Id == model.CategoryId);
                dto.CategoryName = catDto.Name;

                db.SaveChanges();
            }

            //TempData message
            TempData["SM"] = "Edytowałeś produkt";

            #region Image Upload

            //check input file
            if (file != null && file.ContentLength > 0)
            {

                //chceck type of file
                string extension = file.ContentType.ToLower();
                if (extension != "image/jpg" &&
                   extension != "image/jpeg" &&
                   extension != "image/pjpeg" &&
                   extension != "image/gif" &&
                   extension != "image/x-png" &&
                   extension != "image/png")
                {
                    using (Db db = new Db())
                    {
                        model.Categories = new SelectList(db.Categories.ToList(), "Id", "Name");
                        ModelState.AddModelError("", "Obraz nie został przesłany - nieprawidłowe rozszerzenie obrazu. ");
                        return View(model);
                    }

                }
                //Cataloge struct
                var originalDirectory = new DirectoryInfo(string.Format("{0}Images\\Uploads", Server.MapPath(@"\")));

                var pathStrings1 = Path.Combine(originalDirectory.ToString(), "Products\\" + id.ToString());
                var pathStrings2 = Path.Combine(originalDirectory.ToString(), "Products\\" + id.ToString() + "\\Thumbs");

                //delete the old file from cataloge
                DirectoryInfo di1 = new DirectoryInfo(pathStrings1);
                DirectoryInfo di2 = new DirectoryInfo(pathStrings2);

                foreach (var file2 in di1.GetFiles())
                    file2.Delete();


                foreach (var file3 in di2.GetFiles())
                    file3.Delete();

                //save image to database
                string imageName = file.FileName;

                using(Db db = new Db())
                {
                    ProductDTO dto = db.Products.Find(id);
                    dto.ImageName = imageName;
                    db.SaveChanges();

                }

                //save a new files 
                var path = string.Format("{0}\\{1}", pathStrings1, imageName);
                var path2 = string.Format("{0}\\{1}", pathStrings2, imageName);

                //file save
                file.SaveAs(path);

                //for miniatures(thumbs)
                WebImage img = new WebImage(file.InputStream);
                img.Resize(200, 200);
                img.Save(path2);



            }

            #endregion

            return RedirectToAction("EditProduct");
        }

        // GET:Admin/Shop/DeleteProduct
        [HttpGet]
        public ActionResult DeleteProduct(int id)
        {
            //Remove product from data Base
            using(Db db= new Db())
            {
                ProductDTO dto = db.Products.Find(id);
                db.Products.Remove(dto);
                db.SaveChanges();
            }

            //Remove all folder with images
            var originalDirector = new DirectoryInfo(string.Format("{0}Images\\Uploads", Server.MapPath(@"\")));  // Server.MapPath(@"\"))) -> it a path to main folder in aplication
            var pathString = Path.Combine(originalDirector.ToString(), "Products\\" + id.ToString());

            if (Directory.Exists(pathString))
                Directory.Delete(pathString, true);

            return RedirectToAction("Products");
        }

        // POST:Admin/Shop/SaveGalleryImages/id
        [HttpPost]
        public ActionResult SaveGalleryImages(int id)
        {
            //looping for images
            foreach (string fileName in Request.Files)
            {
                //init
                HttpPostedFileBase file = Request.Files[fileName];

                //check existing file a null value
                if(file !=null && file.ContentLength > 0)
                {
                    //
                    var originalDirector = new DirectoryInfo(string.Format("{0}Images\\Uploads", Server.MapPath(@"\")));  // Server.MapPath(@"\"))) -> it a path to main folder in aplication
                    
                    //path for images
                    string pathString1 = Path.Combine(originalDirector.ToString(), "Products\\" + id.ToString() + "\\Gallery");
                    //path for thumbs
                    string pathString2 = Path.Combine(originalDirector.ToString(), "Products\\" + id.ToString() + "\\Gallery\\Thumbs");

                    //single image
                    string pathImage = String.Format("{0}\\{1}", pathString1, file.FileName);
                    //single thumb
                    string pathThumb = String.Format("{0}\\{1}", pathString2, file.FileName);

                    //save to file
                    file.SaveAs(pathImage);
                    //convert to thumb
                    WebImage img = new WebImage(file.InputStream);
                    img.Resize(200, 200);
                    //save thumb ti file
                    img.Save(pathThumb);
                }

            }

            return View();
        }

        // POST:Admin/Shop/DeleteImage
        [HttpPost]
        public void DeleteImage(int id, string imageName)
        {
            //Paths
            string fullPath1 = Request.MapPath("~/Images/Uploads/Products/" + id.ToString() + "/Gallery/" + imageName);
            string fullPath2 = Request.MapPath("~/Images/Uploads/Products/" + id.ToString() + "/Gallery/Thumbs/" + imageName);

            //delete image
            if (System.IO.File.Exists(fullPath1))
                System.IO.File.Delete(fullPath1);
            //delete thumb
            if (System.IO.File.Exists(fullPath2))
                System.IO.File.Delete(fullPath2);

        }

        //Show all orders in Admin Panel
        // GET:Admin/Shop/Orders
        [HttpGet]  
        public ActionResult Orders()
        {
            //initialization Order for admin VM
            List<OrdersForAdminVM> ordersForAdminVM = new List<OrdersForAdminVM>();

            using (Db db = new Db())
            {
                //fetch all orders to list
                List<OrderVM> orders = db.Orders.ToArray().Select(x => new OrderVM(x)).ToList();

                //iterate for orders
                foreach (var order in orders)
                {

                    //initialization products dictionary
                    Dictionary<string, int> productsAndQty = new Dictionary<string, int>();
                    //
                    decimal total = 0m;

                    //initialization ordersDetailsDTO
                    List<OrderDetailsDTO> orderDetailsList = db.OrderDetails.Where(x => x.OrderId == order.OrderId).ToList();

                    //fetch user
                    UserDTO user = db.Users.Where(x => x.Id == order.UserId).FirstOrDefault();
                    string username = user.UserName;

                    foreach (var orderDetails in orderDetailsList)
                    {
                        //fetch product
                        ProductDTO product = db.Products.Where(x => x.Id == orderDetails.ProductId).FirstOrDefault();
                        //fetch price od product
                        decimal price = product.Price;
                        //fetch product name
                        string productName = product.Name;

                        //add product to dictonary
                        productsAndQty.Add(productName, orderDetails.Quantity);

                        //total
                        total += orderDetails.Quantity * price; 
                    }
                    ordersForAdminVM.Add(new OrdersForAdminVM()
                    {
                        OrderNumber = order.OrderId,
                        UserName = user.UserName,
                        Total = total,
                        ProductsAndQty = productsAndQty,
                        CreateAt = order.CreateAt
                    });
                }

            }

           

                return View(ordersForAdminVM);
        }
    }
}