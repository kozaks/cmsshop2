﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CmsShop
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            //for Users Account
            routes.MapRoute("Account", "Account/{action}/{id}", new { controller = "Account", action = "Index", id = UrlParameter.Optional }, new[] { "CmsShop.Controllers" });
            //for Cart(partial)
            routes.MapRoute("Cart", "Cart/{action}/{id}", new { Controller = "Cart", action = "Index", id = UrlParameter.Optional }, new[] { "CmsShop.Controllers" });
            //for Categories(partial)
            routes.MapRoute("Shop", "Shop/{action}/{name}", new { Controller = "Shop", action = "Index", name = UrlParameter.Optional }, new[] { "CmsShop.Controllers" });
            //for SideBar(partial)
            routes.MapRoute("SidebarPartial", "Pages/SidebarPartial", new { Controller = "Pages", action = "SidebarPartial" }, new[] { "CmsShop.Controllers" });
            //for Pages(partial)
            routes.MapRoute("PagesMenuPartial", "Pages/PagesMenuPartial", new { Controller = "Pages", action = "PagesMenuPartial" }, new[] { "CmsShop.Controllers" });
            //for another(seting by user)
            routes.MapRoute("Pages", "{page}", new { Controller = "Pages", action = "Index" }, new[] { "CmsShop.Controllers" });
            //set Roting for default Action
            routes.MapRoute("Default", "", new { Controller = "Pages", action = "Index" }, new[] { "CmsShop.Controllers" });
          

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //);
        }
    }
}
