﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CmsShop.Models.Data
{
    [Table("tblOrderDetails")]
    public class OrderDetailsDTO
    {
        [Key]
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int UserID { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }


        [ForeignKey("UserID")]
        public virtual UserDTO users { get; set; }
        [ForeignKey("OrderId")]
        public virtual OrderDTO orders { get; set; }
        [ForeignKey("ProductId")]
        public virtual ProductDTO products { get; set; }
    }
}