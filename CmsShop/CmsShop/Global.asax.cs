﻿using CmsShop.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace CmsShop
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        //Role service
        protected void Application_AuthenticateRequest()
        {
            if(User == null)
            {
                return;
            }

            //fetch user name from form
            string username = Context.User.Identity.Name;
            //declare role array
            string[] roles = null;

            using(Db db = new Db())
            {
                //fetch data for user from db
                UserDTO dto = db.Users.FirstOrDefault(x => x.UserName == username);
                //fetch roles names
                roles = db.UserRoles.Where(x => x.UserId == dto.Id).Select(x => x.Role.Name).ToArray();
            }

            //create IPrincipal object
            IIdentity userIdentity = new GenericIdentity(username);
            IPrincipal newUserObj = new GenericPrincipal(userIdentity, roles);

            //update Context.user
            Context.User = newUserObj;
        }
    }
}
