﻿using CmsShop.Models.Data;
using CmsShop.Models.ViewModels.Cart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace CmsShop.Controllers
{
    public class CartController : Controller
    {
        // GET: Cart
        public ActionResult Index()
        {
            //initialization Cart
            //if Seesion exist  we assign value as List<CartVM>  else we create  new List<CartVM>();  
            var cart = Session["cart"] as List<CartVM> ?? new List<CartVM>(); 

            //chceck if cart is empty
            if(cart.Count == 0 || Session["cart"] == null)
            {
                ViewBag.Message = "Twój koszyk jest pusty";
            }
            //else..
            //Sum value of assume cart and redirect value to viewBag
            decimal total = 0;

            foreach (var item in cart)
            {
                total += item.Total;
            }

            ViewBag.GrandTotal = total;

            return View(cart);
        }


        //Create Cart in partial view
        public ActionResult CartPartial()
        {
            //initialization cartVM
            CartVM model = new CartVM();

            //initializaction quantity and price
            int quantity = 0;
            decimal price = 0;

            //check if the cart is save in session?
            if(Session["cart"] != null)
            {
                //fetch value from (existing)Session
                var list = (List<CartVM>)Session["cart"];

                foreach (var item in list)
                {
                    //sum value from session
                    quantity += item.Quantity;
                    price += item.Quantity * item.Price;

                }

                model.Quantity = quantity;
                model.Price = price;
            }
            //if session is empty
            else
            {
                //set zero for quantity and price
                quantity = 0;
                price = 0m;
            }

            return PartialView(model);
        }

        public ActionResult AddToCartPartial(int id)
        {
            //initualization CartVM List
            List<CartVM> cart = Session["cart"] as List<CartVM> ?? new List<CartVM>();
            //initializatiom  cartVM
            CartVM model = new CartVM();

            using(Db db = new Db())
            {
                //fetch product
                ProductDTO product = db.Products.Find(id);
                //check existng product in Cart
                var productInCart = cart.FirstOrDefault(x => x.ProductId == id);
                //In depend of if the product is in Cart, we Add product to cart or increment Quantity
                if (productInCart == null)
                {
                    cart.Add(new CartVM()
                    {
                        ProductId = product.Id,
                        ProductName = product.Name,
                        Quantity = 1,
                        Price = product.Price,
                        Image = product.ImageName
                    }); 
                }
                else  //product exist in cart
                {
                    productInCart.Quantity++;
                }
            }

            //fetch sum of values for quantity and price
            int quantity = 0;
            decimal price = 0m;

            foreach (var item in cart)
            {
                quantity += item.Quantity;
                price += item.Quantity * item.Price;
            }

            //set value at the model
            model.Quantity = quantity;
            model.Price = price;

            //save in seesion
            Session["cart"] = cart;

          

            return PartialView(model);
        }

        //increment quantity product(s) in Cart
        public JsonResult IncrementProduct(int productId)
        {
            //init list
            List<CartVM> cart = Session["cart"] as List<CartVM>;
            //fetch cartVm
            CartVM model = cart.FirstOrDefault(x => x.ProductId == productId);

            //increment quantity od product
            model.Quantity++;

            //prepatre json data 
            var result = new { qty = model.Quantity, price = model.Price };

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        //Decrement  quantity product(s) in Cart
        public ActionResult DecrementProduct(int productId)
        {

            //init list
            List<CartVM> cart = Session["cart"] as List<CartVM>;
            //fetch cartVm
            CartVM model = cart.FirstOrDefault(x => x.ProductId == productId);

            //Decrement quantity od product
            if(model.Quantity > 1)
            {
                //decrement if more than zero
                model.Quantity--;
            }

            else
            {
                //if zero == zero
                model.Quantity = 0;
                //remov product from cart
                cart.Remove(model);
            }
            

            //prepatre json data 
            var result = new { qty = model.Quantity, price = model.Price };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public void RemoveProduct(int productId)
        {
            //init list
            List<CartVM> cart = Session["cart"] as List<CartVM>;
            //fetch cartVm
            CartVM model = cart.FirstOrDefault(x => x.ProductId == productId);

            cart.Remove(model);
        }

        public ActionResult PaypalPartial()
        {
            List<CartVM> cart = Session["cart"] as List<CartVM>;

            return PartialView(cart);
        }

        [HttpPost]
        public void PlaceOrder()
        {

            //fetch Cart content
            List<CartVM> cart = Session["cart"] as List<CartVM>;

            //fetch user name
            string username = User.Identity.Name;

            //declare number of order
            int orderId = 0;

            using(Db db = new Db())
            {
                //init OrderDTO
                OrderDTO orderDTO = new OrderDTO();
                //fetch user id
                var user = db.Users.FirstOrDefault(x => x.UserName == username);
                int userId = user.Id;
                //set orderDTO and save to db
                orderDTO.UserId = userId;
                orderDTO.CreateAt = DateTime.Now;

                db.Orders.Add(orderDTO);
                db.SaveChanges();

                //Fetch id sending order
                orderId = orderDTO.OrderId;

                //init OrderDetrails
                OrderDetailsDTO orderDetailsDTO = new OrderDetailsDTO();

                foreach (var item in cart)
                {
                    orderDetailsDTO.OrderId = orderId;
                    orderDetailsDTO.UserID = userId;
                    orderDetailsDTO.ProductId = item.ProductId;
                    orderDetailsDTO.Quantity = item.Quantity;

                    db.OrderDetails.Add(orderDetailsDTO);
                    db.SaveChanges();
                }
                
            }

            //sending email to Admin
            var client = new SmtpClient("smtp.mailtrap.io", 2525)
            {
                Credentials = new NetworkCredential("fc52bb2dd7aa79", "b99439a7fcc65c"),
                EnableSsl = true
            };
            client.Send("admin@example.com", "admin@example.com", "Nowe zamówienie", "Masz nowe zmówienie. NUmer zamówienia " + orderId);

            //reset Session
            Session["cart"] = null;

        }
    }


}