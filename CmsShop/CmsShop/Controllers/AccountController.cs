﻿using CmsShop.Models.Data;
using CmsShop.Models.ViewModels.Account;
using CmsShop.Models.ViewModels.Shop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace CmsShop.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            return Redirect("~/account/login");
        }

        // GET: /account/login
        [HttpGet]
        public ActionResult Login()
        {
            //check if user is log in?
            string userName = User.Identity.Name; //we get it from framework
            //if is log in ...
            if (!string.IsNullOrEmpty(userName))
            {
                return RedirectToAction("user-profile");
            }
                

            //if not logged ...
            return View();
        }

        // POST: /account/login
        [HttpPost]
        public ActionResult Login(LoginUserVM model)
        {
            //modelstate
            if(!ModelState.IsValid)
            {
                return View(model);
            }

            //check users
            bool isValid = false; //if users is approve -> idsValid = true
            using(Db db = new Db())
            {
                //check
                if(db.Users.Any(x => x.UserName.Equals(model.UserName) && x.Password.Equals(model.Password)))
                {
                    isValid = true; //ok
                }
            }
            if (!isValid)
            {
                ModelState.AddModelError("", "Nieprawidłowa nazwa użytkownika lub hasło");
                return View(model);
            }
            else
            {
                FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                return Redirect(FormsAuthentication.GetRedirectUrl(model.UserName, model.RememberMe));
            }

            
        }

        // GET: /account/logout
        [Authorize]
        [HttpGet]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return Redirect("~/account/login");
        }

        // GET: /account/create-account
        [ActionName("create-account")]
        public ActionResult CreateAccount()
        {
            return View("CreateAccount");
        }

        // POST: /account/create-account
        [ActionName("create-account")]
        [HttpPost]
        public ActionResult CreateAccount(UserVM model)
        {
            //check model state valid
            if (!ModelState.IsValid)
            {
                return View("CreateAccount", model);
            }

            //Checking password
            if (!model.Password.Equals(model.ConfirmPassword))
            {
                ModelState.AddModelError("", "Hasła nie pasują do siebie");
                return View("CreateAccount", model);

            }

            using(Db db = new Db())
            {
                //check if userName is unique
                if(db.Users.Any(x => x.UserName.Equals(model.UserName)))
                {
                    ModelState.AddModelError("", "Nazwa użytkownika" + model.UserName + " jest już zajęta");
                    model.UserName = "";
                    return View("CreateAccount", model);
                }

                //else(not unique) - we ceate a new Users account
                UserDTO userDTO = new UserDTO()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    EmailAdress = model.EmailAdress,
                    UserName = model.UserName,
                    Password = model.Password
                };

                //Add user to db
                db.Users.Add(userDTO);
                //save on db
                db.SaveChanges();

                //Adding User Role for new user
                UserRoleDTO userRoleDTO = new UserRoleDTO()
                {
                    UserId = userDTO.Id,
                    RoleId = 2  //2 -> user
                };
                //Add role 
                db.UserRoles.Add(userRoleDTO);
                //save on db
                db.SaveChanges();

            }

            //TempData message
            TempData["SM"] = "Rejestracja przebiegła pomyślnie. Możesz się teraz zalogować";
            return Redirect("~/account/login");
        }

        [Authorize]
        public ActionResult UserNavPartial()
        {
            //fetch userName
            string username = User.Identity.Name;

            //declare model
            UserNavPartialVM model;

            using(Db db = new Db())
            {
                //fetch user
                UserDTO dto = db.Users.FirstOrDefault(x => x.UserName == username);

                model = new UserNavPartialVM()
                {
                    FirstName = dto.FirstName,
                    LastName = dto.LastName
                };
            }

            return PartialView(model);
        }

        [Authorize]
        public ActionResult UserNavPartial2()
        {
            //get username
            string username = User.Identity.Name;

            //declare model
            UserNavPartialVM model;

            using (Db db = new Db())
            {
                UserDTO dto = db.Users.FirstOrDefault(x => x.UserName == username);

                //build te model
                model = new UserNavPartialVM()
                {
                    FirstName = dto.FirstName,
                    LastName = dto.LastName

                };
            }

            return PartialView(model);
        }

        [Authorize]
        public ActionResult UserNavPartial3()
        {
            //get user name
            string username = User.Identity.Name;
            string email = string.Empty;

            //declare model
            UserNavPartialVM3 model;

            using(Db db = new Db())
            {
                //get the user
                UserDTO dto = db.Users.FirstOrDefault(x => x.UserName == username);

                //Build the model
                model = new UserNavPartialVM3()
                {
                    Email = dto.EmailAdress
                };
            }

            //return partial view with model
            return PartialView(model);
        }



        // GET: /account/user-profie
        [HttpGet]
        [ActionName("user-profile")]
        [Authorize]
        public ActionResult UserProfile()
        {
            //fetch user name from form
            string username = User.Identity.Name;
            //declare model
            UserProfileVM model;

            using(Db db = new Db())
            {
                //fetch this user
                UserDTO dto = db.Users.FirstOrDefault(x => x.UserName == username);

                model = new UserProfileVM(dto);
            }

            return View("UserProfile", model);
        }

        // POST: /account/user-profie
        [HttpPost]
        [ActionName("user-profile")]
        [Authorize]
        public ActionResult UserProfile(UserProfileVM model)
        {
            //model state
            if (!ModelState.IsValid)
            {
                return View("UserProfile", model);
            }

            //checking password(Checking vm model = form validation)
            if (!string.IsNullOrWhiteSpace(model.Password))
            {
                if (!model.Password.Equals(model.ConfirmPassword))
                {
                    ModelState.AddModelError("", "Hasła nie pasują do siebie.");
                    return View("UserProfile", model);

                }
            }

            //db checking
            using (Db db = new Db())
            {
                //fetch user name from form(moddel)
                string username = User.Identity.Name;

                //check unique usename
                if (db.Users.Where(x => x.Id != model.Id).Any(x => x.UserName == username))
                {
                    ModelState.AddModelError("", "Nazwa użytkownika" + model.UserName + "zajęta");
                    model.UserName = "";
                    return View("UserProfile", model);
                }

                //Edit DTO
                UserDTO dto = db.Users.Find(model.Id);
                dto.FirstName = model.FirstName;
                dto.LastName = model.LastName;
                dto.EmailAdress = model.EmailAdress;
                dto.UserName = model.UserName;

                //checking if password is empty
                if (!string.IsNullOrWhiteSpace(model.Password))
                {
                    dto.Password = model.Password;
                }

                db.SaveChanges();

            }

            //TempData message
            TempData["SM"] = "Edytowałeś swój profil";

            return Redirect("~/account/user-profile");
        }

        //View all orders for User
        // GET: /account/Orders
        [Authorize(Roles="User")]
        public ActionResult Orders()
        {

            //init list of orders for user
            List<OrdersForUserVM> ordersForUser = new List<OrdersForUserVM>();

            using (Db db = new Db())
            {
                //geting users id by itendity - for one value firstOrDefault
                UserDTO user = db.Users.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
                //user id
                int userId = user.Id;

      

                //fetch order for login user
                List<OrderVM> orders = db.Orders.Where(x => x.UserId == userId).ToArray().Select(x => new OrderVM(x)).ToList();
              

                foreach (var order in orders)
                {
                    int userr = order.UserId; 
                    //init deictonary of products
                    Dictionary<string, int> productsAndQty = new Dictionary<string, int>();
                    decimal total = 0;
                    //fetch orderDetailsonly to collection(without VM)
                    List<OrderDetailsDTO> orderDetailsDTO = db.OrderDetails.Where(x => x.OrderId == order.OrderId).ToList();
                    
                    foreach (var orderdetail in orderDetailsDTO)
                    {
                        //fetch product
                        ProductDTO product = db.Products.Where(x => x.Id == orderdetail.ProductId).FirstOrDefault();
                        //price of product
                        decimal price = product.Price;
                        string productName = product.Name;

                        productsAndQty.Add(productName, orderdetail.Quantity);

                        total += orderdetail.Quantity * price;
                    }

                    ordersForUser.Add(new OrdersForUserVM
                    {
                        OrderNumber = order.OrderId,
                        Total = total,
                        ProductsAndQty = productsAndQty,
                        CreateAt = order.CreateAt
                    }); ;
                }
            }

            return View(ordersForUser);

           
        }
    }
}