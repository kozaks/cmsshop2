﻿using CmsShop.Models.Data;
using CmsShop.Models.ViewModels.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CmsShop.Controllers
{
    public class PagesController : Controller
    {
        // GET: Index/{pages}
        public ActionResult Index(string page = "")
        {
            //string page = "" - describe default page(for example "home")

            //Set slug page
            //so if not declare by user ...
            if (page == "")
                page = "home";
            //Declare PageVm, PageDTO
            PageVM model;
            PageDTO dto;

            //check existing page
            using(Db db = new Db())
            {
                if(!db.Pages.Any(x => x.Slug.Equals(page)))               
                    //not existing case - redirect to default page(main page)
                    return RedirectToAction("Index", new { page = "" });
                
            }

            //fetch pageDTo
            using(Db db = new Db())
            {
                dto = db.Pages.Where(x => x.Slug == page).FirstOrDefault();
            }

            //set title page
            ViewBag.PageTitle = dto.Title;

            //CHeck if page has SIdeBar
            if(dto.HasSidebar == true)
                ViewBag.Sidebar = "Tak";
            
            else
                ViewBag.Sidebar = "Nie";

            

            //Initialization PageVm
            model = new PageVM(dto);


            return View(model);
        }

        public ActionResult PagesMenuPartial()
        {
            //declare PageVm
            List<PageVM> pageVMList;
            //fetch pages
            using (Db db = new Db())
            {
                pageVMList = db.Pages.ToArray()
                    .OrderBy(x => x.Sorting)
                    .Where(x => x.Slug != "home")
                    .Select(x => new PageVM(x)).ToList();
            }

            //return list of PageeVM
            return PartialView(pageVMList);
            
        }

        public ActionResult SidebarPartial()
        {
            //declare a model
            SidebarVM model;
            //initialization model
            using(Db db = new Db())
            {
                SidebarDTO dto = db.Sidebar.Find(1);
                model = new SidebarVM(dto);
            }

            //return partial with model
            return PartialView(model);
        }
    }
}