﻿using CmsShop.Models.Data;
using CmsShop.Models.ViewModels.Shop;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CmsShop.Controllers
{
    public class ShopController : Controller
    {
        // GET: Shop
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Pages");
        }

        //display a shop categories
        public ActionResult CategoryMenuPartial()
        {
            //declare CategoryVm List
            List<CategoryVM> categoryVMList;
            //nitialization list
            using(Db db = new Db())
            {

                categoryVMList = db.Categories
                    .ToArray()
                    .OrderBy(x => x.Sorting)
                    .Select(x => new CategoryVM(x))
                    .ToList();
            }

            return PartialView(categoryVMList);
        }

        //return list o products when user click on Category in SideBar
        // GET: Shop/category/name
        public ActionResult Category(string name)
        {
            //declare productVM list
            List<ProductVM> productVMList;
            //
            using(Db db = new Db())
            {
                //fetch id selected Category(select all object)
                CategoryDTO categoryDTO = db.Categories.Where(x => x.Slug == name).FirstOrDefault();
                //select id
                int catId = categoryDTO.Id;
                //init list of Product(in selected category)
                productVMList = db.Products
                    .ToArray()
                    .Where(x => x.CategoryId == catId)
                    .Select(x => new ProductVM(x)).ToList();

                //fetch category name
                var productCat = db.Products.Where(x => x.CategoryId == catId).FirstOrDefault();
                ViewBag.CategoryName = productCat.CategoryName;


            }

            return View(productVMList);
        }

        // GET: Shop/product-szczegoly/name
        [ActionName("product-szczegoly")]
        public ActionResult ProductDetails(string name)
        {
            //declaretion ProductVm, productDTO
            ProductVM model;
            ProductDTO dto;
            //initialization id
            int id = 0;
            
            using(Db db = new Db())
            {
                //chcecking if product exist
                if(!db.Products.Any(x => x.Slug == name))
                {
                    return RedirectToAction("Index", "Shop");
                }

                //init prductDTO
                dto = db.Products.Where(x => x.Slug == name).FirstOrDefault();

                //fetch id
                id = dto.Id;

                //init model
                model = new ProductVM(dto);
            }
            //fetch gallery to choosen product
            model.GalleryImages = Directory.EnumerateFiles(Server.MapPath("~/Images/Uploads/Products/" + id + "/Gallery/Thumbs"))
                                          .Select(fn => Path.GetFileName(fn));

            return View("ProductDetails", model);
        }
    }
}